<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Responsive Bootstrap Creative HTML Template">
    <meta name="keywords" content="industry Pro, Bootstrap, creative Template, minimal, agency, corporate">
    <meta name="author" content="rashed amin">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- SITE TITLE -->
    <title>industry pro</title>
    <!-- =========================
        FAV AND TOUCH ICONS (RETINA)
        ============================== -->
    <link rel="icon" href="images/favicon.ico">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="apple-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="apple-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="apple-icon-152x152.png" />
    <link rel="icon" type="image/png" href="images/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="images/favicon-16x16.png" sizes="16x16" />
    <meta name="msapplication-TileImage" content="ms-icon-144x144.png" />
    <!-- =========================
         STYLESHEETS
         ============================== -->
    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- FONT ICONS -->
    <link rel="stylesheet" href="assets/elegant-icons/style.css">
    <link rel="stylesheet" href="assets/et-line-font/style.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.css">
    <link rel="stylesheet" href="assets/ion-icon/ionicons.min.css">
    <!-- <link rel="stylesheet" href="assets/app-icons/styles.css"> -->
    <!--[if lte IE 7]><script src="lte-ie7.js"></script><![endif]-->

    <!-- CAROUSEL AND LIGHTBOX -->
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/nivo-lightbox.css">
    <link rel="stylesheet" href="css/nivo_themes/default/default.css">
    <!-- ANIMATIONS -->
    <link rel="stylesheet" href="css/animate.min.css">
    <link href="css/bootsnav.css" rel="stylesheet">

    <!-- MAIN STYLESHEETS -->
    <link rel="stylesheet" href="css/styles.css">
    <!-- COLORS -->
    <link rel="stylesheet" href="css/color-schemes/paste.css">
    <!-- RESPONSIVE FIXES -->
    <!-- <link rel="stylesheet" href="css/responsive.css"> -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- =========================
     PRE LOADER
     ============================== -->
<div class="preloader">
    <div id="ip-container" class="ip-container">
        <div class="ip-header">
            <div class="ip-loader">
                <svg class="ip-inner" width="60px" height="60px" viewBox="0 0 80 80">
                    <path class="ip-loader-circlebg" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                    <path id="ip-loader-circle" class="ip-loader-circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                </svg>
            </div>
        </div>
    </div>
</div>
<!-- =========================
     HEADER
     ============================== -->
<header class="header" id="home">
    <!-- HEADER COLOR OVER IMAGE -->
    <nav class="navbar navbar-default navbar-fixed white no-background bootsnav">
        <!-- Start Top Search -->
        <div class="top-search">
            <div class="container">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <input type="text" class="form-control" placeholder="Search">
                    <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                </div>
            </div>
        </div>
        <!-- End Top Search -->
        <div class="container">
            <!-- Start Atribute Navigation -->
            <div class="attr-nav">
                <ul>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-shopping-bag"></i>
                            <span class="badge">3</span>
                        </a>
                        <ul class="dropdown-menu cart-list animated">
                            <li>
                                <a href="#" class="photo"><img src="images/thumb/thumb01.jpg" class="cart-thumb" alt=""></a>
                                <h6><a href="#">Delica omtantur </a></h6>
                                <p>2x - <span class="price">$99.99</span></p>
                            </li>
                            <li>
                                <a href="#" class="photo"><img src="images/thumb/thumb02.jpg" class="cart-thumb" alt=""></a>
                                <h6><a href="#">Omnes ocurreret</a></h6>
                                <p>1x - <span class="price">$33.33</span></p>
                            </li>
                            <li>
                                <a href="#" class="photo"><img src="images/thumb/thumb03.jpg" class="cart-thumb" alt=""></a>
                                <h6><a href="#">Agam facilisis</a></h6>
                                <p>2x - <span class="price">$99.99</span></p>
                            </li>
                            <li class="total">
                                <span class="pull-right"><strong>Total</strong>: $0.00</span>
                                <a href="#" class="btn btn-default btn-cart">Cart</a>
                            </li>
                        </ul>
                    </li>
                    <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
                </ul>
            </div>
            <!-- End Atribute Navigation -->
            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="#"><img src="images/logo.png" class="logo" alt=""></a>
            </div>
            <!-- End Header Navigation -->
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-right" data-in="" data-out="">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">home</a>
                        <ul class="dropdown-menu animated ">
                            <li>
                                <a href="index.html" class="dropdown-toggle" data-toggle="dropdown">home 1</a>
                            </li>
                            <li>
                                <a href="index-2.html" class="dropdown-toggle" data-toggle="dropdown">home 2</a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="about.html" class="dropdown-toggle" data-toggle="dropdown">about</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">service</a>
                        <ul class="dropdown-menu animated ">
                            <li>
                                <a href="service-1.html" class="dropdown-toggle" data-toggle="dropdown">service 1</a>
                            </li>
                            <li>
                                <a href="service-2.html" class="dropdown-toggle" data-toggle="dropdown">service 2</a>
                            </li>



                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Project</a>
                        <ul class="dropdown-menu animated ">
                            <li>
                                <a href="project-1.html" class="dropdown-toggle" data-toggle="dropdown">Project 1</a>
                            </li>
                            <li>
                                <a href="project-2.html" class="dropdown-toggle" data-toggle="dropdown">Project 2</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="blog-list-01.html">Blog</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Shop</a>
                        <ul class="dropdown-menu animated ">
                            <li>
                                <a href="shop-page.html" class="dropdown-toggle" data-toggle="dropdown">Shop Page</a>
                            </li>
                            <li>
                                <a href="shop-details.html" class="dropdown-toggle" data-toggle="dropdown">Single Product</a>
                            </li>
                            <li>
                                <a href="shopping-cart.html" class="dropdown-toggle" data-toggle="dropdown">Shopping Cart</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="contact.html" class="dropdown-toggle" data-toggle="dropdown">contact</a>
                    </li>

                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>

        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

            <div class="item active"> <!-- Slider Item -->
                <img src="images/construct/slider5.jpg" alt="background-image">
                <div class="carousel-color-overlay">
                    <div class="carousel-caption">
                        <div class="intro-section">
                            <h1 class="intro hs4-h1 animated slideInDown animation-delay-6 text-white-7">Industry Pro<br><span>born to build.</span></h1>

                        </div>

                    </div>
                </div>
            </div>

            <div class="item"> <!-- Slider Item -->
                <img src="images/construct/slider6.jpg" alt="background-image">
                <div class="carousel-color-overlay">
                    <div class="carousel-caption">
                        <div class="intro-section">
                            <h1 class="intro hs4-h1 animated slideInRight animation-delay-6 text-white-7">we build<br><span>smart house for you.</span></h1>

                        </div>
                    </div>
                </div>
            </div> <!-- End Slider item -->


            <div class="item"> <!-- Slider Item -->
                <img src="images/construct/slider7.jpg" alt="background-image">
                <div class="carousel-color-overlay">
                    <div class="carousel-caption">
                        <div class="intro-section">
                            <h1 class="intro hs4-h1 animated slideInRight animation-delay-6 text-white-7">we create<br><span>eco friendly product.</span></h1>

                        </div>
                    </div>
                </div>
            </div> <!-- End Slider item -->



        </div> <!-- End Carousel Inner -->

    </div>  <!-- End carousel -->

</header>
<!-- /END HEADER -->
<!-- =========================
     ABOUT US
     ============================== -->
<section class="section-top " id="about">
    <div class="container">
        <div class="row">



            <!-- ABOUT SECTION BIO -->
            <div class="about-us">
                <div class="col-md-offset-1 col-md-6">

                    <h2 class="constr-about-title">We are <b>Industry Pro</b></h2>
                    <div class="constr-under-line"></div>
                    <p class="constr-home-abt">Building construction is the process of adding structure to real property or construction of buildings. The majority of building construction jobs are small renovations, such as addition of a room.<br>The majority of building construction jobs are small renovations, such as addition of a room.</p>

                    <div class="res-funfactor" id="funfactor">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="icon-container">
                                        <i class="icon-wallet "></i>
                                    </div>
                                    <div class="fun-info">
                                        <h2 class="timer numbers" data-from="0" data-to="1620"> 1620</h2>
                                        <span class="fun-work">happy clients</span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="icon-container">
                                        <i class="icon-briefcase"></i>
                                    </div>
                                    <div class="fun-info">
                                        <h2 class="timer numbers" data-from="0" data-to="1290"> 390 </h2>
                                        <span class="fun-work">project finished</span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="icon-container">
                                        <i class="icon-gift"></i>
                                    </div>
                                    <div class="fun-info">
                                        <h2 class="timer numbers" data-from="0" data-to="778">278 </h2>
                                        <span class="fun-work">case solved</span>
                                    </div>
                                </div>

                            </div> <!-- END ROW -->
                        </div>

                    </div>

                </div>
            </div><!-- ABOUT SECTION END -->

            <div class="row">

                <div class="col-md-5 col-sm-6 constr-about-image">
                    <div class="constr-abt-image-1 ">
                        <img src="images/construct/about-1.jpg" alt="constr">
                    </div>
                    <div class="constr-abt-image-2">
                        <img class="constr-abt-img" src="images/construct/about-2.jpg" alt="Constr">
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- END ROW -->
    </div>
</section>
<!-- ABOUT US  SECTION END -->
<!-- =========================
     SERVICES
     ============================== -->
<section class="services constr-service" id="services">
    <div class="container">

        <!-- SERVICE SECTION HEADER -->
        <div class="section-header-1">
            <!--  SECTION DESCRIPTION -->
            <h4 class="section-description">
                we offer
            </h4>
        </div>
        <!-- /END SERVICE SECTION HEADER -->
        <div class="row">
            <div class="tab-head">
                <nav class="col-md-3 nav-sidebar" >
                    <ul class="nav tabs">
                        <li class="active"><a href="#tab1" data-toggle="tab">BUILDING ENGINEERING</a></li>
                        <li class=""><a href="#tab2" data-toggle="tab">CONSTRUCTION PLAN</a></li>
                        <li class=""><a href="#tab3" data-toggle="tab"> RENOVATION</a></li>
                        <li class=""><a href="#tab4" data-toggle="tab">MAINTANANCE</a></li>
                        <li class=""><a href="#tab5" data-toggle="tab">ROOF-FLOR REPAIRING</a></li>
                        <li class=""><a href="#tab6" data-toggle="tab">SOIL CONSULTANTCY</a></li>
                        <li class=""><a href="#tab7" data-toggle="tab"> ACHITECHTURE</a></li>
                    </ul>
                </nav>
                <div class="col-md-9 tab-content">
                    <div class="tab-pane active text-style" id="tab1">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="facts">
                                    <img src="images/construct/s-1.png" alt="">
                                    <h4>BUILDING ENGINEERING</h4>
                                    <p> There are many variations of passages of Lorem Ipsum available, Nullam ut consectetur dolor. Sed sit amet iaculis nisi. Nullam ut consectetur dolor amet iaculis nisi. but the majority have suffered alteration in some form, by injected humour
                                        or randomised words.</p>
                                    <a href="construction-service.html">read more</a>
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    </div>
                    <div class="tab-pane  text-style" id="tab2">
                        <div class="facts">
                            <img src="images/construct/s-2.png" alt="">
                            <h4> PRECONSTRUCTION PLANNING</h4>
                            <p> Breakfast agreeable incommode departure it an. By ignorant at on wondered relation. Enough at tastes really so cousin am of. Extensive therefore supported by extremity of contented. Breakfast agreeable incommode departure it an. By ignorant at on wondered relation. Enough at tastes really so cousin am of. Extensive therefore supported by extremity of contented</p>
                            <a href="construction-service.html">read more</a>

                        </div>
                    </div>
                    <div class="tab-pane  text-style" id="tab3">
                        <div class="facts">
                            <img src="images/construct/s-3.png" alt="">
                            <h4>HOUSE AND RENOVATION</h4>
                            <p> Started several mistake joy say painful removed reached end. State burst think end are its. Arrived off she elderly beloved him affixed noisier yet. An course regard to up he hardly. View four has said does men saw find dear shy.</p>
                            <a href="construction-service.html">read more</a>

                        </div>
                    </div>
                    <div class="tab-pane text-style" id="tab4">
                        <div class="facts">
                            <img src="images/construct/s-4.png" alt="">
                            <h4>BUILDING MAINTANANCE</h4>
                            <p> Knowledge nay estimable questions repulsive daughters boy. Solicitude gay way unaffected expression for. His mistress ladyship required off horrible disposed rejoiced. Unpleasing pianoforte unreserved as oh he unpleasant no inquietude insipidity. </p>
                            <a href="construction-service.html">read more</a>

                        </div>
                    </div>
                    <div class="tab-pane  text-style" id="tab5">
                        <div class="facts">
                            <img src="images/construct/s-5.png" alt="">
                            <h4>ROOF REPAIRING</h4>
                            <p> Sociable on as carriage my position weddings raillery consider. Peculiar trifling absolute and wandered vicinity property yet. The and collecting motionless difficulty son. His hearing staying ten colonel met.</p>
                            <a href="construction-service.html">read more</a>

                        </div>
                    </div>
                    <div class="tab-pane  text-style" id="tab6">
                        <div class="facts">
                            <img src="images/construct/s-6.png" alt="">
                            <h4>SOIL CONSULTANTCY</h4>
                            <p> An country demesne message it. Bachelor domestic extended doubtful as concerns at. Morning prudent removal an letters by. On could my in order never it. Or excited certain sixteen it to parties colonel.</p>
                            <a href="construction-service.html">read more</a>

                        </div>
                    </div>
                    <div class="tab-pane  text-style" id="tab7">
                        <div class="facts">
                            <img src="images/construct/s-7.png" alt="">
                            <h4>DESIGN & ACHITECHTURE</h4>
                            <p>Unpleasant nor diminution excellence apartments imprudence the met new. Draw part them he an to he roof only. Music leave say doors him. Tore bred form if sigh case as do.</p>
                            <a href="construction-service.html">read more</a>

                        </div>
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>

        </div>
        <!-- /END ROW -->

    </div>
    <!-- /END CONTAINER -->
</section>
<!-- /END SERVICES SECTION -->


<!-- =========================
     Why Choose Us
     ============================== -->
<section class=" services constr-why-bg-color" id="services-1">

    <!-- /END SERVICE SECTION HEADER -->
    <div class="row no-gutter">
        <!-- SINGLE SERVICE -->
        <div class="col-md-4">
            <div class="avatar"> <div class="const-img-border-small-fix  border-black"></div><img src="images/construct/srv-1.jpg" alt="" class="fit-img"> </div>
        </div>
        <div class="col-md-8">
            <div class="const-srv-title">
                Why choose us
            </div>
            <div class="const-srv">
                <div class="col-md-6">
                    <div class="constr-item const-choice text-left m-b-30">
                        <div class="icon constr-bottom-border">
                            <i class="icon-circle-compass"></i>
                        </div>
                        <div class="title text-uppercase">
                            <h4>Innovative design</h4>
                        </div>
                        <div class="desc">
                            Fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor Praesent sed nisi.
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="constr-item const-choice text-left m-b-30">
                        <div class="icon constr-bottom-border">
                            <i class="icon-tools-2"></i>
                        </div>
                        <div class="title text-uppercase">
                            <h4>Expert Team Work</h4>
                        </div>
                        <div class="desc">
                            Fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor Praesent sed nisi.
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="constr-item const-choice text-left m-b-30">
                        <div class="icon constr-bottom-border">
                            <i class="icon-speedometer"></i>
                        </div>
                        <div class="title text-uppercase">
                            <h4>fast Building</h4>
                        </div>
                        <div class="desc">
                            Fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor Praesent sed nisi.
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="constr-item const-choice text-left m-b-30">
                        <div class="icon constr-bottom-border">
                            <i class="icon-edit"></i>
                        </div>
                        <div class="title text-uppercase">
                            <h4>Committed Timeline </h4>
                        </div>
                        <div class="desc">
                            Fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor Praesent sed nisi.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /END ROW -->

    </div>
    <!-- /END CONTAINER -->
</section>
<!-- /END SERVICES SECTION -->


<!-- =========================
     FEATURE WORK
     ============================== -->
<section class="feature-top mt-80">
    <div class="  nopadding">
        <div class="row">
            <!-- PORTFOLIO SECTION HEADER -->
            <div class="section-header-1">
                <!--  SECTION TITLE -->
                <h4>Feature projects</h4>


            </div>
            <!-- /END PORTFOLIO HEADER -->
            <!-- PORTFOLIO ITEMS START -->
            <div class="col-md-12">

                <!-- PORTFOLIO ITEMS -->
                <div id="even-grid">
                    <div class="isotope">
                        <div class="grids-item  width2 item nopadding photo craft">
                            <img class="grid-item"  src="images/construct/1.jpg" alt="portfolio">
                            <!-- Portfolio Hover -->
                            <div class="portfolio-hover">
                                <div class="action-btn">
                                    <!-- LightBox Button -->
                                    <div class="port-title">industry Pro city</div>
                                    <a class="port-subt" href="construction-project-details.html">Convention hall</a>
                                </div>
                            </div>
                            <!-- End Portfolio Hover -->
                        </div>

                        <div class="grids-item item height2 nopadding constr-bg-color">
                            <div class="title text-uppercase">
                                <h5 class="constr-ftr-subtitle">Creating values</h5>
                                <div class="colored-line"> </div>
                                <h4 class="constr-ftr-title">Industry Pro <br>Construction</h4>
                            </div>
                            <div class="constr-ftr-desc">
                                Industry Construction has developed most sustainable and most smart building through worlwide.
                            </div>
                        </div>
                        <div class="grids-item item nopadding photo craft">
                            <img class="grid-item"  src="images/construct/2.jpg" alt="portfolio">
                            <!-- Portfolio Hover -->
                            <div class="portfolio-hover">
                                <div class="action-btn">
                                    <div class="port-title">tower h2</div>
                                    <a class="port-subt" href="construction-project-details.html">apartments</a>
                                </div>
                            </div>
                            <!-- End Portfolio Hover -->
                        </div>
                        <div class="grids-item item nopadding craft web-solution">
                            <img class="grid-item"  src="images/construct/3.jpg" alt="portfolio">
                            <!-- Portfolio Hover -->
                            <div class="portfolio-hover">
                                <div class="action-btn">
                                    <!-- LightBox Button -->
                                    <div class="port-title">tower a2</div> <br>
                                    <a class="lightbox" data-lightbox-gallery="gallery1" href="images/construct/3.jpg"> <i class="icon-magnifying-glass"></i> </a>
                                    <!-- End LightBox Button -->
                                </div>
                            </div>
                            <!-- End Portfolio Hover -->
                        </div>
                        <div class="grids-item item nopadding branding web-solution">
                            <img class="grid-item"  src="images/construct/4.jpg" alt="portfolio">
                            <!-- Portfolio Hover -->
                            <div class="portfolio-hover">
                                <div class="action-btn">
                                    <!-- LightBox Button -->
                                    <div class="port-title">bridge monera</div>
                                    <a class="port-subt" href="construction-project-details.html">construction</a>
                                </div>
                            </div>
                            <!-- End Portfolio Hover -->
                        </div>
                        <div class="grids-item item nopadding branding web-solution">
                            <img class="grid-item"  src="images/construct/5.jpg" alt="portfolio">
                            <!-- Portfolio Hover -->
                            <div class="portfolio-hover">
                                <div class="action-btn">
                                    <div class="port-title">ninja park</div> <br>
                                    <a class="lightbox" data-lightbox-gallery="gallery1" href="images/construct/5.jpg"> <i class="icon-magnifying-glass"></i> </a>
                                </div>
                            </div>
                            <!-- End Portfolio Hover -->
                        </div>
                    </div> <!-- END PORTFOLIO ITEMS -->
                </div>
            </div>
        </div> <!-- END ROW -->
    </div>
</section>


<!-- =========================
     CALL TO ACTION
     ============================== -->
<!--full width promo default box start-->
<div class="full-width promo-box promo-bg-color m-bot-50">
    <div class="container">
        <div class="col-md-12">
            <div class="promo-info">
                <div class="promo-info-t">Looking for experts for your next construction?</div>
                <span>Nullam ut consectetur dolor. Sed sit amet iaculis nisi. Mauris ridiculus elementum non felis etewe blandit. </span>
            </div>
            <div class="promo-btn">
                <a href="#" class="btn btn-medium btn-rounded btn-dark-solid  text-uppercase">contact now</a>
            </div>
        </div>
    </div>
</div>
<!--full width promo default box end-->

<!-- =========================
TESTIMONIALS
============================== -->

<section class=" testimonial-3">

    <div class="container">
        <div class="row">

            <div class="section-header-1">
                <!--  SECTION DESCRIPTION -->
                <h4 class="section-description">
                    feedback
                </h4>
            </div>

            <div class="testimonials-3">

                <div class="divider d-solid d-single "> </div>

                <div class="container wow fadeIn animated" data-wow-offset="10" data-wow-duration="1.5s">

                    <!-- FEEDBACKS  STARTS-->
                    <div id="feedbacks" class="owl-carousel owl-theme">

                        <!-- SINGLE FEEDBACK -->
                        <div class="feedback">

                            <!-- IMAGE -->
                            <div class="image">
                                <!-- i class=" icon_quotations"></i -->
                                <img src="images/clients-pic/c-1.jpg" alt="">
                            </div>

                            <div class="message">
                                Keep your face to the sunshine and you cannot see a shadow.Find a place inside where there's joy, and the joy will burn out the pain.Live life to the fullest, and focus on the positive.


                            </div>

                            <div class="black-line">
                            </div>

                            <!-- INFORMATION -->
                            <div class="name">
                                Phillip Morris.
                            </div>
                            <div class="company-info">
                                Creation Studio.
                            </div>

                        </div>
                        <!-- /END SINGLE FEEDBACK -->

                        <!-- SINGLE FEEDBACK -->
                        <div class="feedback">

                            <!-- IMAGE -->
                            <div class="image">
                                <!-- i class=" icon_quotations"></i -->
                                <img src="images/clients-pic/c-2.jpg" alt="">
                            </div>

                            <div class="message">
                                Once you replace negative thoughts with positive ones, you'll start having positive results.Live life to the fullest, and focus on the positive.Keep your face to the sunshine and you cannot see a shadow.
                            </div>

                            <div class="black-line">
                            </div>

                            <!-- INFORMATION -->
                            <div class="name">
                                Christina.
                            </div>
                            <div class="company-info">
                                Christina Collections.
                            </div>

                        </div>
                        <!-- /END SINGLE FEEDBACK -->

                        <!-- SINGLE FEEDBACK -->
                        <div class="feedback">

                            <!-- IMAGE -->
                            <div class="image">
                                <!-- i class=" icon_quotations"></i -->
                                <img src="images/clients-pic/c-3.jpg" alt="">
                            </div>
                            <!-- MESSAGE -->
                            <div class="message">
                                Once you replace negative thoughts with positive ones, you'll start having positive results.Live life to the fullest, and focus on the positive.Keep your face to the sunshine and you cannot see a shadow.
                            </div>

                            <div class="black-line">
                            </div>

                            <!-- INFORMATION -->
                            <div class="name">
                                Hughman.
                            </div>
                            <div class="company-info">
                                Oley.
                            </div>

                        </div>
                        <!-- /END SINGLE FEEDBACK -->

                    </div>
                    <!-- /END FEEDBACKS -->

                </div>
                <!-- /END CONTAINER -->
                <div class="divider d-solid d-single "> </div>

            </div>
        </div>  <!-- END ROW -->
    </div> <!-- END CONTAINER -->
</section>
<!-- /END TESTIMONIALS SECTION -->
<!-- =========================
     CLIENTS
     ============================== -->
<div class="clients">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- CLIENTS LOGO -->
                <div id="clients-1">
                    <div class="item"><img src="images/clients/c-1.png" alt="Clients"></div>
                    <div class="item"><img src="images/clients/c-2.png" alt="Clients"></div>
                    <div class="item"><img src="images/clients/c-7.png" alt="Clients"></div>
                    <div class="item"><img src="images/clients/c-9.png" alt="Clients"></div>
                    <div class="item"><img src="images/clients/c-10.png" alt="Clients"></div>
                    <div class="item"><img src="images/clients/c-6.png" alt="Clients"></div>
                    <div class="item"><img src="images/clients/c-7.png" alt="Clients"></div>
                    <div class="item"><img src="images/clients/c-8.png" alt="Clients"></div>
                </div>
            </div>
        </div>  <!-- END ROW -->
    </div>    <!-- END CONTAINER -->
</div>


<!-- =========================
     FOOTER
     ============================== -->
<footer class="footer-section footer-1">
    <div class="row footer-content">
        <div class="col-md-6 footer-column">
            <!-- footer logo -->
            <div class="footer-logo">
                <img src="images/logo-white.png" alt="logo" />
                <p>industry Pro</p>
            </div>
            <div class="footer-desc">
                Once you replace negative thoughts with positive ones, Keep your face to the sunshine and you cannot see a shadow.
            </div>
            <!-- COPYRIGHT TEXT -->
            <div class="copyright-3">&copy; theCreo. All Rights Reserved.</div>
        </div>
        <div class="col-md-3 footer-column">
            <ul class="footer__nav-list">
                <li class="footer__nav-item footer__nav-item--heading">Company</li>
                <li class="footer__nav-item"><a href="#about" class="footer__nav-link">About</a></li>
                <li class="footer__nav-item"><a href="blog-list-01.html" class="footer__nav-link">Blog</a></li>
                <li class="footer__nav-item"><a href="#contact-us" class="footer__nav-link">Contact</a></li>
                <li class="footer__nav-item"><a href="#policy" class="footer__nav-link">Privacy Policy</a></li>
            </ul>
        </div>

        <div class="col-md-3">
            <ul class="footer__nav-list">
                <li class="footer__nav-item footer__nav-item--heading">Contact</li>
                <li>123-456 Montserrat La videon <br>
                    Corsa, Roma-1245, Italia.</li>

                <li class="email-3">support@creo.com</li>
                <li class="footer__nav-item social-li"><a href="#" class="footer__nav-link">TWITTER</a><a href="#" class="footer__nav-link">FACEBOOK</a><a href="#" class="footer__nav-link">INSTAGRAM</a></li>
            </ul>
        </div>

    </div>
    <!-- SCROLL UP -->
    <div class="scroll-up">
        <a class="theme-color-bg" href="#home"><i class="fa fa-angle-up"></i></a>
    </div>


</footer>
<!-- /END FOOTER -->
<!-- =========================
     SCRIPTS
     ============================== -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-migrate-3.0.0.js"></script>
<link href="css/owl.carousel.css" rel="stylesheet">
<script src="js/owl.carousel.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="js/modernizr.custom.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootsnav.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jquery.localScroll.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/parallax.min.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/matchMedia.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery.countTo.js"></script>
<script src="js/nivo-lightbox.min.js"></script>
<script src="js/classie.js"></script>
<script src="js/jquery.ajaxchimp.js"></script>
<script src="js/pathLoader.js"></script>
<script src="js/circles.min.js"></script>
<script src="js/piecharts-2.js"></script>
<script src="js/custom.js"></script>
</body>
</html>
