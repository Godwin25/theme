

<!doctype>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Seamfix -
        Solutions</title>
    <link rel="stylesheet" type="text/css" href="https://seamfix.com/wp-content/themes/seamfix2017-temp/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://seamfix.com/wp-content/themes/seamfix2017-temp/style.css"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Mono:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">


    <!-- This site is optimized with the Yoast SEO plugin v4.4 - https://yoast.com/wordpress/plugins/seo/ -->
    <link rel="canonical" href="https://seamfix.com/solutions/" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Solutions - Seamfix" />
    <link  type="text/css" media="all" rel="stylesheet"  href="https://seamfix.com/wp-content/plugins/wp-share-button/themes/theme17/style.css" ><meta property="og:url" content="https://seamfix.com/solutions/" />
    <meta property="og:site_name" content="Seamfix" />
    <meta property="og:image" content="https://seamfix.com/wp-content/uploads/2017/03/samuel-zeller-34751-copy.png" />
    <meta property="og:image:width" content="1280" />
    <meta property="og:image:height" content="350" />
    <meta name="twitter:card" content="summary" />
    <link  type="text/css" media="all" rel="stylesheet"  href="https://seamfix.com/wp-content/plugins/wp-share-button/themes/theme17/style.css" ><meta name="twitter:title" content="Solutions - Seamfix" />
    <meta name="twitter:image" content="https://seamfix.com/wp-content/uploads/2017/03/samuel-zeller-34751-copy.png" />
    <!-- / Yoast SEO plugin. -->

    <link rel='dns-prefetch' href='//s.w.org' />
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/seamfix.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7.5"}};
        !function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='contact-form-7-css'  href='https://seamfix.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.7' type='text/css' media='all' />
    <link rel='stylesheet' id='rs-plugin-settings-css'  href='https://seamfix.com/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.3.2' type='text/css' media='all' />
    <style id='rs-plugin-settings-inline-css' type='text/css'>
        #rs-demo-id {}
    </style>
    <link rel='stylesheet' id='svt-style-css'  href='https://seamfix.com/wp-content/plugins/simple-vertical-timeline/css/simple-vertical-timeline.min.css?ver=4.7.5' type='text/css' media='all' />
    <link rel='stylesheet' id='svt-linearicons-css'  href='https://seamfix.com/wp-content/plugins/simple-vertical-timeline/img/linearicons/style.css?ver=4.7.5' type='text/css' media='all' />
    <link rel='stylesheet' id='dashicons-css'  href='https://seamfix.com/wp-includes/css/dashicons.min.css?ver=4.7.5' type='text/css' media='all' />
    <link rel='stylesheet' id='to-top-css'  href='https://seamfix.com/wp-content/plugins/to-top/public/css/to-top-public.css?ver=1.0' type='text/css' media='all' />
    <link rel='stylesheet' id='wp_share_button_style-css'  href='https://seamfix.com/wp-content/plugins/wp-share-button/css/style.css?ver=4.7.5' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-css'  href='https://seamfix.com/wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/font-awesome.min.css?ver=5.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='jquery-ui-css'  href='https://seamfix.com/wp-content/plugins/wp-share-button/admin/css/jquery-ui.css?ver=4.7.5' type='text/css' media='all' />
    <link rel='stylesheet' id='recent-posts-widget-with-thumbnails-public-style-css'  href='https://seamfix.com/wp-content/plugins/recent-posts-widget-with-thumbnails/public.css?ver=5.0' type='text/css' media='all' />
    <link rel='stylesheet' id='js_composer_front-css'  href='https://seamfix.com/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='ebs_dynamic_css-css'  href='https://seamfix.com/wp-content/plugins/easy-bootstrap-shortcodes/styles/ebs_dynamic_css.php?ver=4.7.5' type='text/css' media='all' />
    <!-- This site uses the Google Analytics by MonsterInsights plugin v6.1.10 - Using Analytics tracking - https://www.monsterinsights.com/ -->
    <script type="text/javascript" data-cfasync="false">
        /* Function to detect opted out users */
        function __gaTrackerIsOptedOut() {
            return document.cookie.indexOf(disableStr + '=true') > -1;
        }

        /* Disable tracking if the opt-out cookie exists. */
        var disableStr = 'ga-disable-UA-40872224-1';
        if ( __gaTrackerIsOptedOut() ) {
            window[disableStr] = true;
        }

        /* Opt-out function */
        function __gaTrackerOptout() {
            document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
            window[disableStr] = true;
        }

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

        __gaTracker('create', 'UA-40872224-1', 'auto');
        __gaTracker('set', 'forceSSL', true);
        __gaTracker('require', 'displayfeatures');
        __gaTracker('require', 'linkid', 'linkid.js');
        __gaTracker('send','pageview');
    </script>
    <!-- / Google Analytics by MonsterInsights -->
    <script type='text/javascript' src='https://seamfix.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
    <script type='text/javascript' src='https://seamfix.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var monsterinsights_frontend = {"js_events_tracking":"true","is_debug_mode":"false","download_extensions":"doc,exe,js,pdf,ppt,tgz,zip,xls","inbound_paths":"","home_url":"https:\/\/seamfix.com","track_download_as":"event","internal_label":"int","hash_tracking":"false"};
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://seamfix.com/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=6.1.10'></script>
    <script type='text/javascript' src='https://seamfix.com/wp-content/plugins/simple-vertical-timeline/js/svt-animation.min.js?ver=4.7.5'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var to_top_options = {"scroll_offset":"100","icon_opacity":"50","style":"icon","icon_type":"dashicons-arrow-up-alt2","icon_color":"#ffffff","icon_bg_color":"#000000","icon_size":"32","border_radius":"5","image":"https:\/\/seamfix.com\/wp-content\/plugins\/to-top\/admin\/images\/default.png","image_width":"65","image_alt":"","location":"bottom-right","margin_x":"20","margin_y":"20","show_on_admin":"0","enable_autohide":"0","autohide_time":"2","enable_hide_small_device":"0","small_device_max_width":"640","reset":"0"};
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://seamfix.com/wp-content/plugins/to-top/public/js/to-top-public.js?ver=1.0'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wp_share_button_ajax = {"wp_share_button_ajaxurl":"https:\/\/seamfix.com\/wp-admin\/admin-ajax.php"};
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://seamfix.com/wp-content/plugins/wp-share-button/js/scripts.js?ver=4.7.5'></script>
    <link rel='https://api.w.org/' href='https://seamfix.com/wp-json/' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://seamfix.com/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://seamfix.com/wp-includes/wlwmanifest.xml" />
    <link rel='shortlink' href='https://seamfix.com/?p=149' />
    <link rel="alternate" type="application/json+oembed" href="https://seamfix.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fseamfix.com%2Fsolutions%2F" />
    <link rel="alternate" type="text/xml+oembed" href="https://seamfix.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fseamfix.com%2Fsolutions%2F&#038;format=xml" />
    <meta property="og:title" content="Solutions" /><meta property="og:url" content="https://seamfix.com/solutions/" /><meta property="og:image" content="https://seamfix.com/wp-content/uploads/2017/03/samuel-zeller-34751-copy.png" />		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
    <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://seamfix.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.4.3.2 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
    <link rel="icon" href="https://seamfix.com/wp-content/uploads/2017/04/Seamfix-logo.png" sizes="32x32" />
    <link rel="icon" href="https://seamfix.com/wp-content/uploads/2017/04/Seamfix-logo.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="https://seamfix.com/wp-content/uploads/2017/04/Seamfix-logo.png" />
    <meta name="msapplication-TileImage" content="https://seamfix.com/wp-content/uploads/2017/04/Seamfix-logo.png" />
    <script type="text/javascript">function setREVStartSize(e){
            try{ var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
                if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
            }catch(d){console.log("Failure at Presize of Slider:"+d)}
        };</script>
    <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1492675230605{background-position: center !important;background-repeat: no-repeat !important;background-size: contain !important;}.vc_custom_1492517478411{padding-top: 7% !important;padding-bottom: 7% !important;background-image: url(https://seamfix.com/wp-content/uploads/2017/04/fuel.png?id=279) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1492517400390{background-image: url(https://seamfix.com/wp-content/uploads/2017/04/2017-04-10.png?id=262) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1492517493229{background-image: url(https://seamfix.com/wp-content/uploads/2017/04/transcript.png?id=288) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1492517501790{background-image: url(https://seamfix.com/wp-content/uploads/2017/04/kyc.png?id=299) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>


<section class="other-hero" style="background:linear-gradient(
        rgba(11,57,81,0.5),
        rgba(11,57,81,0.7)
        ), url(https://seamfix.com/wp-content/uploads/2017/03/samuel-zeller-34751-copy.png); background-size: cover;">
    <div class="container-fluid">

        <div class="row p-55"  data-spy="affix" data-offset-top="197">
            <div class="col-md-3 col-xs-5">
                <div class="logo-pos">
                    <a href="https://seamfix.com"><img src="https://seamfix.com/wp-content/themes/seamfix2017-temp/img/logo.svg" class="img-responsive"  style="cursor: pointer"/></a>
                </div>
            </div>

            <div class="col-md-9 col-xs-7">
                <nav class="navbar navbar-inverse" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul id="menu-primary-nav" class="nav navbar-nav  navbar-right"><li id="menu-item-5" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-5 dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Company <span class="caret"></span></a>
                                <ul role="menu" class=" dropdown-menu">
                                    <li id="menu-item-473" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-473"><a href="https://seamfix.com/who-we-are/">Who We Are</a></li>
                                    <li id="menu-item-492" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-492"><a href="https://seamfix.com/leadership/">Leadership</a></li>
                                </ul>
                            </li>
                            <li id="menu-item-296" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-149 current_page_item menu-item-has-children menu-item-296 dropdown active"><a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Solutions <span class="caret"></span></a>
                                <ul role="menu" class=" dropdown-menu">
                                    <li id="menu-item-467" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-467"><a href="https://seamfix.com/solutions/auto-topup/">AutoTopUp</a></li>
                                    <li id="menu-item-468" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-468"><a href="https://seamfix.com/solutions/fuelvoucher/">FuelVoucher</a></li>
                                    <li id="menu-item-469" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-469"><a href="https://seamfix.com/solutions/itranscript/">iTranscript</a></li>
                                    <li id="menu-item-470" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-470"><a href="https://seamfix.com/solutions/biosmart-kyc/">BioSmart KYC</a></li>
                                    <li id="menu-item-471" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-471"><a href="https://seamfix.com/solutions/loanpair/">LoanPair</a></li>
                                    <li id="menu-item-472" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-472"><a href="https://seamfix.com/solutions/verified-ng/">Verified.ng</a></li>
                                    <li id="menu-item-493" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-493"><a href="https://seamfix.com/solutions/bioregistra/">BioRegistra</a></li>
                                </ul>
                            </li>
                            <li id="menu-item-306" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-306 dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Resources <span class="caret"></span></a>
                                <ul role="menu" class=" dropdown-menu">
                                    <li id="menu-item-603" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-603"><a href="https://seamfix.com/resources/product-brochures/">Product Brochures</a></li>
                                    <li id="menu-item-602" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-602"><a href="https://seamfix.com/resources/press-kit/">Press Kit</a></li>
                                </ul>
                            </li>
                            <li id="menu-item-8" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-8 dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Our Thinking <span class="caret"></span></a>
                                <ul role="menu" class=" dropdown-menu">
                                    <li id="menu-item-476" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-476"><a href="https://seamfix.com/industry-insight/">Industry Insights</a></li>
                                    <li id="menu-item-475" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-475"><a href="https://seamfix.com/blog/">Blog</a></li>
                                </ul>
                            </li>
                            <li id="menu-item-474" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-474"><a href="https://seamfix.com/careers/">Careers</a></li>
                            <li id="menu-item-297" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-297"><a href="https://seamfix.com/contact/">Contact</a></li>
                        </ul>            </div>
                </nav>
            </div>
        </div>        <div class="row">
            <div class="line1">
                <h1>Solutions</h1>
            </div>
            <div class="line2"></div>
            <div class="line3">
                <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
                    <!-- Breadcrumb NavXT 5.7.0 -->
                    <span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" href="https://seamfix.com" class="home"><span property="name">Home</span></a><meta property="position" content="1"></span> &gt; <span property="itemListElement" typeof="ListItem"><span property="name">Solutions</span><meta property="position" content="2"></span>
                </div>
            </div>
        </div>

</section>

<div class="container">
    <link  type="text/css" media="all" rel="stylesheet"  href="https://seamfix.com/wp-content/plugins/wp-share-button/themes/theme17/style.css" ><section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section gradient-product wpb_animate_when_almost_visible wpb_fadeIn fadeIn vc_custom_1492675230605 vc_section-has-fill"><div class="vc_row wpb_row vc_row-fluid wpb_animate_when_almost_visible wpb_fadeIn fadeIn"><div class="p-55 wfont wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper">
                        <div  class="wpb_single_image wpb_content_element vc_align_left">

                            <figure class="wpb_wrapper vc_figure">
                                <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="196" height="53" src="https://seamfix.com/wp-content/uploads/2017/04/LoanPair-Logo.png" class="vc_single_image-img attachment-medium" alt="" /></div>
                            </figure>
                        </div>

                        <div class="wpb_text_column wpb_content_element " >
                            <div class="wpb_wrapper">
                                <div class="pb-55">
                                    <h2>Enjoy convenient and trustworthy lending with LoanPair.</h2>
                                    <p>&nbsp;</p>
                                    <h4>Flexible Terms</h4>
                                    <h4>Negotiate your interest rates</h4>
                                    <h4>Automated Payback Process</h4>
                                    <p>&nbsp;</p>
                                    <a class="btn btn-default btn-lg btn-round " href="loanpair"  rel=""  target="_self">Learn more</a></div>

                            </div>
                        </div>
                    </div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper">
                        <div  class="wpb_single_image wpb_content_element vc_align_left   img-responsive">

                            <figure class="wpb_wrapper vc_figure">
                                <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="385" height="603" src="https://seamfix.com/wp-content/uploads/2017/04/hand.png" class="vc_single_image-img attachment-full" alt="" srcset="https://seamfix.com/wp-content/uploads/2017/04/hand.png 385w, https://seamfix.com/wp-content/uploads/2017/04/hand-192x300.png 192w" sizes="(max-width: 385px) 100vw, 385px" /></div>
                            </figure>
                        </div>
                    </div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element " >
                            <div class="wpb_wrapper">
                                <div class="pb-95" style="color: #fff;">
                                    <h2><b>Make a Rewarding Move today</b></h2>
                                </div>

                            </div>
                        </div>
                    </div></div></div></div></section><div class="vc_row-full-width vc_clearfix"></div><section class="vc_section no-padding"><div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid flexbox vc_row-no-padding"><div class="no-padding wpb_animate_when_almost_visible wpb_slideInLeft slideInLeft wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill"><div class="vc_column-inner vc_custom_1492517400390"><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid flexcol p-5"><div class="col-md-offset-4 col-md-8 wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">
                                        <div  class="wpb_single_image wpb_content_element vc_align_left">

                                            <figure class="wpb_wrapper vc_figure">
                                                <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="252" height="65" src="https://seamfix.com/wp-content/uploads/2017/04/verified2.png" class="vc_single_image-img attachment-full" alt="" /></div>
                                            </figure>
                                        </div>
                                        <div class="vc_empty_space"   style="height: 6%" ><span class="vc_empty_space_inner"></span></div>

                                        <div class="wpb_text_column wpb_content_element  wfont" >
                                            <div class="wpb_wrapper">
                                                <h2><span style="color: #5cb85b;">Real-time Identity</span><br />
                                                    <span style="color: #5cb85b;">&amp; Document Verification</span></h2>

                                            </div>
                                        </div>
                                        <div class="vc_empty_space"   style="height: 6%" ><span class="vc_empty_space_inner"></span></div>

                                        <div class="wpb_text_column wpb_content_element  wfont" >
                                            <div class="wpb_wrapper">
                                                <div class="row "><div class="col-lg-7 col-md-12 col-xs-12 col-sm-12"></p>
                                                        <h3>Confirm Identities at Superfast Speed against multiple Integrated Databases through the VERIFIED Web Platform or API.</h3>
                                                        <p>&nbsp;</p>
                                                        <pre><code></code></pre>
                                                        <p></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div></div></div></div></div></div></div><div class="wpb_animate_when_almost_visible wpb_slideInRight slideInRight wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid flexcol p-5"><div class="col-md-6 wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner "><div class="wpb_wrapper">
                                        <div  class="wpb_single_image wpb_content_element vc_align_left">

                                            <figure class="wpb_wrapper vc_figure">
                                                <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="252" height="65" src="https://seamfix.com/wp-content/uploads/2017/04/2017-04-10-1.png" class="vc_single_image-img attachment-full" alt="" /></div>
                                            </figure>
                                        </div>
                                        <div class="vc_empty_space"   style="height: 6%" ><span class="vc_empty_space_inner"></span></div>

                                        <div class="wpb_text_column wpb_content_element " >
                                            <div class="wpb_wrapper">
                                                <h2>Industry-grade Data Capture, Analytics &amp; Identity Management for Every Budget.</h2>

                                            </div>
                                        </div>
                                        <div class="vc_empty_space"   style="height: 6%" ><span class="vc_empty_space_inner"></span></div>

                                        <div class="wpb_text_column wpb_content_element " >
                                            <div class="wpb_wrapper">
                                                <div class="row "><div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"></p>
                                                        <h3><span style="color: #5cb85b;">Capture, Store, Analyse &amp; Migrate your data on your terms.</span></h3>
                                                        <p>&nbsp;</p>
                                                        <p><a class="btn btn-success btn-lg btn-round " href="kyc"  rel=""  target="_self">Learn more</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div></section><section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section wpb_animate_when_almost_visible wpb_fadeIn fadeIn vc_custom_1492517478411 vc_section-has-fill"><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper">
                        <div  class="wpb_single_image wpb_content_element vc_align_left">

                            <figure class="wpb_wrapper vc_figure">
                                <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="252" height="65" src="https://seamfix.com/wp-content/uploads/2017/04/FuelVoucher.png" class="vc_single_image-img attachment-medium" alt="" /></div>
                            </figure>
                        </div>
                        <div class="vc_empty_space"   style="height: 7%" ><span class="vc_empty_space_inner"></span></div>

                        <div class="wpb_text_column wpb_content_element " >
                            <div class="wpb_wrapper">
                                <h1><span style="color: #ffffff;"><strong>Buy Fuel Online.</strong></span><br />
                                    <span style="color: #ffffff;"><strong>Fill up Anywhere.</strong></span></h1>

                            </div>
                        </div>
                        <div class="vc_empty_space"   style="height: 7%" ><span class="vc_empty_space_inner"></span></div>

                        <div class="wpb_text_column wpb_content_element " >
                            <div class="wpb_wrapper">
                                <h3><span style="color: #ffffff;">We focus on your fueling needs so you can focus on other important things!</span></h3>

                            </div>
                        </div>
                        <div class="vc_empty_space"   style="height: 7%" ><span class="vc_empty_space_inner"></span></div>

                        <div class="wpb_text_column wpb_content_element  wfont" >
                            <div class="wpb_wrapper">
                                <a class="btn btn-default btn-lg btn-round " href="fuelvoucher"  rel=""  target="_self">Learn more</a>

                            </div>
                        </div>
                    </div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div></div></section><div class="vc_row-full-width vc_clearfix"></div><section class="vc_section no-padding"><div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid flexbox no-padding vc_row-no-padding vc_row-o-equal-height vc_row-flex"><div class="no-padding flexcol wpb_animate_when_almost_visible wpb_slideInLeft slideInLeft wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill"><div class="vc_column-inner vc_custom_1492517493229"><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid p-5"><div class="col-md-offset-4 col-md-8 wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner "><div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element  wfont" >
                                            <div class="wpb_wrapper">
                                                <h1><span style="color: #ffffff;">iTranscript</span></h1>

                                            </div>
                                        </div>
                                        <div class="vc_empty_space"   style="height: 6%" ><span class="vc_empty_space_inner"></span></div>

                                        <div class="wpb_text_column wpb_content_element  wfont" >
                                            <div class="wpb_wrapper">
                                                <h2><span style="color: #ffffff;">Digitise your students’ record and provide effective administrative experience.</span></h2>

                                            </div>
                                        </div>
                                        <div class="vc_empty_space"   style="height: 6%" ><span class="vc_empty_space_inner"></span></div>

                                        <div class="wpb_text_column wpb_content_element  wfont" >
                                            <div class="wpb_wrapper">
                                                <div class="row "><div class="col-lg-10 col-md-12 col-xs-12 col-sm-12"></p>
                                                        <h4><span style="color: #ffffff;">With iTranscript, your students’ data and your school’s reputation are much safer.</span></h4>
                                                        <p>&nbsp;</p>
                                                        <p>&nbsp;</p>
                                                        <p><a class="btn btn-default btn-lg btn-round " href="itranscript"  rel=""  target="_self">Learn more</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div></div></div></div></div><div class="flexcol wpb_animate_when_almost_visible wpb_slideInRight slideInRight wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill"><div class="vc_column-inner vc_custom_1492517501790"><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid p-5"><div class="col-md-8 wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner "><div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element  wfont" >
                                            <div class="wpb_wrapper">
                                                <h1><span style="color: #ffffff;">KYC</span></h1>

                                            </div>
                                        </div>
                                        <div class="vc_empty_space"   style="height: 6%" ><span class="vc_empty_space_inner"></span></div>

                                        <div class="wpb_text_column wpb_content_element  wfont" >
                                            <div class="wpb_wrapper">
                                                <h2><span style="color: #ffffff;">Know your Customer</span></h2>

                                            </div>
                                        </div>
                                        <div class="vc_empty_space"   style="height: 6%" ><span class="vc_empty_space_inner"></span></div>

                                        <div class="wpb_text_column wpb_content_element  wfont" >
                                            <div class="wpb_wrapper">
                                                <div class="row "><div class="col-lg-10 col-md-12 col-xs-12 col-sm-12"></p>
                                                        <h4><span style="color: #ffffff;">Robust server engine interacting with several interconnected systems to deliver seamless customer/subscriber registration</span></h4>
                                                        <p>&nbsp;</p>
                                                        <p>&nbsp;</p>
                                                        <p><a class="btn btn-default btn-lg btn-round " href="kyc"  rel=""  target="_self">Learn more</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div></section><section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section no-padding"></section><div class="vc_row-full-width vc_clearfix"></div>
</div>

<footer class="pb-95" style="padding-top:0;">
    <div class="container">
        <!--<div class="row">
            <div class="col-md-12">
                <img src="https://seamfix.com/wp-content/themes/seamfix2017-temp/img/icon.png" class="img-responsive center-block">
            </div>
        </div>-->
        <div class="row pb-95">
            <div class="col-md-3">
                <b>About Us</b>
                <p class="transparent-footer">
                    <br/>
                    <!--have the depth of technology, expertise and scale to enable you deliver advanced,
                    secure and superior customer experiences and drive competitive advantage.-->
                    <div class="row">
                        <div class="col-md-11">
                <p class="transparent-footer">
                    Seamfix is a leading provider of technology solutions and services in the Nigerian and larger African market. Our systems empower our clients to derive maximum value, deliver value to their customers and stay ahead of the competition.
                </p>
            </div>
        </div>

        </p>
    </div>
    <div class="col-md-2  col-sm-6 col-xs-6">
        <b>Company</b>
        <br/><br/>
        <div class="menu-first-container"><ul id="menu-first" class=""><li id="menu-item-31" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31"><a href="https://seamfix.com/who-we-are/">Who We Are</a></li>
                <li id="menu-item-298" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-298"><a href="https://seamfix.com/leadership/">Leadership</a></li>
                <li id="menu-item-662" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-662"><a href="https://seamfix.com/resources/product-brochures">Product Brochures</a></li>
                <li id="menu-item-661" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-661"><a href="https://seamfix.com/resources/press-kit/">Press Kit</a></li>
            </ul></div>            </div>
    <div class="col-md-2 col-sm-6 col-xs-6">
        <b>Solutions</b>
        <br/><br/>
        <div class="menu-second-container"><ul id="menu-second" class=""><li id="menu-item-495" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-495"><a href="https://seamfix.com/solutions/bioregistra/">BioRegistra</a></li>
                <li id="menu-item-394" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-394"><a href="https://seamfix.com/solutions/verified-ng/">Verified.ng</a></li>
                <li id="menu-item-395" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-395"><a href="https://seamfix.com/solutions/loanpair/">LoanPair</a></li>
                <li id="menu-item-396" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-396"><a href="https://seamfix.com/solutions/itranscript/">iTranscript</a></li>
                <li id="menu-item-537" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-537"><a href="https://seamfix.com/solutions/auto-topup/">AutoTopUp</a></li>
                <li id="menu-item-393" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-393"><a href="https://seamfix.com/solutions/biosmart-kyc/">BioSmart KYC</a></li>
                <li id="menu-item-538" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-538"><a href="https://seamfix.com/solutions/fuelvoucher/">FuelVoucher</a></li>
            </ul></div>            </div>
    <div class="col-md-2  col-sm-6  col-xs-6">
        <b>Our Thinking</b>
        <br/><br/>
        <div class="menu-fourth-container"><ul id="menu-fourth" class=""><li id="menu-item-424" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-424"><a href="https://seamfix.com/industry-insight/">Industry Insights</a></li>
                <li id="menu-item-397" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-397"><a href="https://seamfix.com/blog/">Blog</a></li>
            </ul></div>            </div>
    <div class="col-md-3  col-sm-6  col-xs-12">
        <b>Contact</b>
        <br/><br/>
        <div class="wpb_wrapper">

            <div class="wpb_text_column wpb_content_element ">
                <div class="wpb_wrapper">
                    <div class="media">
                        <div class="media-object pull-left"><i class="fa fa-map-marker"></i>
                        </div>
                        <div class="media-body">
                            <article class="text-justify transparent-footer">1st Floor, Leasing House,</article>
                            <article class="text-justify transparent-footer">C &amp; I Leasing Drive,</article>
                            <article class="text-justify transparent-footer">Off Bisola Durosinmi Etti Drive,</article>
                            <article class="text-justify transparent-footer">Off Admiralty Way, Lekki Phase 1,</article>
                            <article class="text-justify transparent-footer">Lagos, Nigeria</article>
                            <br/>
                        </div>
                    </div>

                </div>
            </div>

            <div class="wpb_text_column wpb_content_element ">
                <div class="wpb_wrapper">
                    <div class="media">
                        <div class="media-object pull-left"><i class="fa fa-phone"></i>
                        </div>
                        <div class="media-body">
                            <article class="text-justify transparent-footer">+234-01-342-9192</article>
                            <br/>
                        </div>
                    </div>

                </div>
            </div>

            <div class="wpb_text_column wpb_content_element ">
                <div class="wpb_wrapper">
                    <div class="media">
                        <div class="media-object pull-left"><i class="fa fa-envelope"></i>
                        </div>
                        <div class="media-body">
                            <article class="text-justify transparent-footer">info@seamfix.com</article>
                        </div>
                    </div>

                </div>
            </div>

            <div class="wpb_text_column wpb_content_element ">
                <div class="wpb_wrapper">
                    <h3><span style="color: #00cb6e;"><a style="color: #00cb6e;" href="https://www.facebook.com/SeamfixLtd" target="_blank"><i class="fa fa-facebook-square"></i></a>&nbsp;<a style="color: #00cb6e;" href="https://twitter.com/seamfixltd" target="_blank"><i class="fa fa-twitter-square"></i></a> <a style="color: #00cb6e;" href="https://www.linkedin.com/company-beta/855861" target="_blank"><i class="fa fa-linkedin-square"></i></a></span></h3>

                </div>
            </div>

        </div>




    </div>

    </div>
    <div class="row" >
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <b>&copy; 2017 SEAMFIX Ltd, All Rights Reserved</b> <div class="menu-terms-container"><ul id="menu-terms" class="menu-terms"><li id="menu-item-517" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-517"><a href="https://seamfix.com/terms-and-conditions/">Terms</a></li>
                            <li id="menu-item-520" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-520"><a href="https://seamfix.com/privacy/">Privacy</a></li>
                        </ul></div>                    </div>
            </div>
        </div>
    </div>

    </div>
</footer>

<script src="https://seamfix.com/wp-content/themes/seamfix2017-temp/js/bootstrap.min.js"></script>
<script src="https://seamfix.com/wp-content/themes/seamfix2017-temp/js/custom.js"></script>


<div id="to_top_scrollup" class="dashicons dashicons-arrow-up-alt2"><span class="screen-reader-text">Scroll Up</span></div><link rel='stylesheet' id='animate-css-css'  href='https://seamfix.com/wp-content/plugins/js_composer/assets/lib/bower/animate-css/animate.min.css?ver=5.1.1' type='text/css' media='all' />
<script type='text/javascript' src='https://seamfix.com/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpcf7 = {"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}},"cached":"1"};
    /* ]]> */
</script>
<script type='text/javascript' src='https://seamfix.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.7'></script>
<script type='text/javascript' src='https://seamfix.com/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.4.3.2'></script>
<script type='text/javascript' src='https://seamfix.com/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.3.2'></script>
<script type='text/javascript' src='https://seamfix.com/wp-content/plugins/revslider/public/assets/js/extensions/revolution.extension.actions.min.js?ver=5.4.3.2'></script>
<script type='text/javascript' src='https://seamfix.com/wp-content/plugins/revslider/public/assets/js/extensions/revolution.extension.carousel.min.js?ver=5.4.3.2'></script>
<script type='text/javascript' src='https://seamfix.com/wp-content/plugins/revslider/public/assets/js/extensions/revolution.extension.kenburn.min.js?ver=5.4.3.2'></script>
<script type='text/javascript' src='https://seamfix.com/wp-content/plugins/revslider/public/assets/js/extensions/revolution.extension.layeranimation.min.js?ver=5.4.3.2'></script>
<script type='text/javascript' src='https://seamfix.com/wp-content/plugins/revslider/public/assets/js/extensions/revolution.extension.migration.min.js?ver=5.4.3.2'></script>
<script type='text/javascript' src='https://seamfix.com/wp-content/plugins/revslider/public/assets/js/extensions/revolution.extension.navigation.min.js?ver=5.4.3.2'></script>
<script type='text/javascript' src='https://seamfix.com/wp-content/plugins/revslider/public/assets/js/extensions/revolution.extension.parallax.min.js?ver=5.4.3.2'></script>
<script type='text/javascript' src='https://seamfix.com/wp-content/plugins/revslider/public/assets/js/extensions/revolution.extension.slideanims.min.js?ver=5.4.3.2'></script>
<script type='text/javascript' src='https://seamfix.com/wp-content/plugins/revslider/public/assets/js/extensions/revolution.extension.video.min.js?ver=5.4.3.2'></script>
<script type='text/javascript' src='https://seamfix.com/wp-includes/js/wp-embed.min.js?ver=4.7.5'></script>
<script type='text/javascript' src='https://seamfix.com/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=5.1.1'></script>
<script type='text/javascript' src='https://seamfix.com/wp-content/plugins/js_composer/assets/lib/waypoints/waypoints.min.js?ver=5.1.1'></script>
</body>
</html>